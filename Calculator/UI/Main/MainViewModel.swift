//
//  MainViewModel.swift
//  Calculator
//
//  Created by Eldar Akkozov on 6/2/22.
//

import Foundation

protocol MainDelegate: AnyObject {
    func showMath(math: String)
    func showResult(result: String)
}

class MainViewModel {
    
    private weak var delegate: MainDelegate? = nil
    
    init(delegate: MainDelegate) {
        self.delegate = delegate
    }
    
    private var math = "0"
    private var operators = ["+", "-", "*", "/", "."]
    
    func clcikBotton(_ title: String) {
        if title == "=" {
            return
        }
        
        if title == "AC" {
            clearLast()
        } else if title == "C" {
            clearAll()
        } else if !(operators.contains(String(math.last ?? Character(""))) && operators.contains(title)) {
            calculatemathResult(title)
        }
    }
    
    private func clearLast() {
        math = String(math.dropLast())
        
        if math.count == 0 {
            math = "0"
        }
        
        calculatorMath(math)
    }
    
    private func clearAll() {
        math = "0"
        
        delegate?.showResult(result: "0")
        delegate?.showMath(math: "0")
    }
    
    private func calculatemathResult(_ title: String) {
        if (String(math.first ?? Character(""))) == "0" {
            math = String(math.dropFirst())
        }
        
        math = math + title
        
        calculatorMath(math)
    }
    
    private func calculatorMath(_ math: String) {
        if !operators.contains(String(math.last ?? Character(""))) {
            let expr = NSExpression(format: math)
            if let result = expr.expressionValue(with: nil, context: nil) as? NSNumber {
                delegate?.showResult(result: String(result.doubleValue))
            } else {
                delegate?.showResult(result: "0")
            }
        } else {
            delegate?.showResult(result: "0")
        }
        
        delegate?.showMath(math: math)
    }
}


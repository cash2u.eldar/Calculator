//
//  ButtonModel.swift
//  Calculator
//
//  Created by Eldar Akkozov on 6/2/22.
//

import Foundation
import UIKit

struct ButtonModel {
    var title: String
    var titlecolor: UIColor
    var color: UIColor
    
    init(_ title: String, _ titlecolor: UIColor, _ color: UIColor) {
        self.title = title
        self.titlecolor = titlecolor
        self.color = color
    }
}
